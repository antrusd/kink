#!/bin/bash

# only set -e when it's not interactive
[[ ! $- =~ i ]] && set -e


ci_echo_red() {
    echo -e "\e[31m"$@"\e[0m"
}

ci_echo_yellow() {
    echo -e "\e[33m"$@"\e[0m"
}
